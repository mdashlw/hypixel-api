package ru.mdashlw.hypixel.api.exceptions

open class HypixelApiException(message: String) : RuntimeException(message)
