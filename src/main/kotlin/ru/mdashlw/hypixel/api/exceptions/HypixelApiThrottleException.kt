package ru.mdashlw.hypixel.api.exceptions

class HypixelApiThrottleException : HypixelApiException("You have passed the API throttle limit!")
