package ru.mdashlw.hypixel.api.interfaces

interface Kit {
    val apiName: String
    val localizedName: String
}
